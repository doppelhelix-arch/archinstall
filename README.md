# Archlinux with btrfs and snapper on GNOME
 This guide assumes that you already booted into your live environment as described in the [Archlinux Installation guide][1].
 
To keep things simple, [`archinstall`][archinstall] is used.

---

<details open>
<summary><b>Table of contents</b></summary>
<p>

[**Part 1: Installation**](#part-1-installation)

1. [Keyboard layout](#keyboard-layout)
2. [Wifi](#wifi)
3. [Start the installer](#start-the-installer)
4. [Language of the installer](#language-of-the-installer)
5. [German language pack](#german-language-pack)
6. [Drive to install the system to (I assume you know your drives?!)](#drive-to-install-the-system-to-i-assume-you-know-your-drives)
7. [Disk layout](#disk-layout)
8. [Bootloader](#bootloader)
9. [Hostname](#hostname)
10. [Root account](#root-account)
11. [User account ](#user-account-)
12. [GNOME and open-source graphic driver](#gnome-and-open-source-graphic-driver)
13. [Audio-backend](#audio-backend)
14. [Kernel](#kernel)
15. [Additional packages](#additional-packages)
16. [Networking](#networking)
17. [Timezone](#timezone)
18. [Optional repositories](#optional-repositories)
19. [Start the installation](#start-the-installation)

[**Part 2: chroot-environment**](#part-2-chroot-environment)

20. [chroot](#chroot)
21. [fstab](#fstab)
22. [Compress btrfs](#compress-btrfs)
23. [systemd services](#systemd-services)
24. [Configure and enable Reflector](#configure-and-enable-reflector)
25. [Exit chroot and reboot](#exit-chroot-and-reboot)

[**Part 3: Inside the new ArchLinux system**](#part-3-inside-the-new-archlinux-system)

26. [Check /etc/fstab](#check-etcfstab)
27. [Update the system](#update-the-system)
28. [KVM/QEMU](#kvmqemu)
29. [Enable Arch User Repository (AUR)](#enable-arch-user-repository-aur)
30. [Create and manage snapshots](#create-and-manage-snapshots)
31. [Fix initial ramdisk](#fix-initial-ramdisk)
32. [Fix snapshots](#fix-snapshots)
33. [Change btrfs default subvolume to @](#change-btrfs-default-subvolume-to-)
34. [Enable swap](#enable-swap)
35. [Change permissions for snapper and /.snapshots](#change-permissions-for-snapper-and-snapshots)
36. [Set snapshot limits](#set-snapshot-limits)
37. [Preventing slowdowns](#preventing-slowdowns)
</p>
</details>

---
## ***Part 1:*** Installation
### Keyboard layout
- This guide is centered around the german language. If you need another keyboard layout, change the commands accordingly.

~~~sh
loadkeys de-latin1
~~~
  
### Wifi
~~~sh
iwctl device list
iwctl station <DEVICE> scan
iwctl station <DEVICE> get-networks
iwctl --passphrase=<PASSPHRASE> station <DEVICE> connect <SSID>
~~~

### Start the installer
~~~sh
archinstall
~~~

### Language of the installer
~~~
Archinstall language = English
~~~

### German language pack
~~~
Keyboard layout = de
Mirror region = Germany
Locale Language = de_DE.UTF-8
Locale encoding = UTF-8
~~~

### Drive to install the system to (I assume you know your drives?!)
~~~
Drives = <CHOOSE_YOUR_INSTALL_DRIVE>
~~~

### Disk layout
~~~
Disk layout = <HERE BE DRAGONS>
~~~
#### First partition
~~~
fat32
  startlocation = 1MiB
  endlocation = 4098MiB
  mountpoint = /boot
~~~
#### Second partition
~~~
btrfs
  startlocation = 4099MiB
  endlocation = 100%
~~~
#### btrfs subvolumes
~~~	      
  Subvolumes 
  #escape out of compress, archinstall doesn't enable it anyway (2023-06-2023)

    @ = /
    @home = /home
    @swap = /swap
    @snapshots = /.snapshots
    @var_log = /var/log
    @pkg = /var/cache/pacman/pkg
    @images = /var/lib/libvirt/images
# IMPORTANT
      nodatacow = yes

# gdm needs to be rw if we boot from snapshots
# This is why we make the following subvolumes
    @AccountsService = /var/lib/AccountsService
    @gdm = /var/lib/gdm
~~~
 ---> The subvolumes `@AccontService` and `@gdm` will ensure that [`gdm`][gdm] works if booted into a [snapshot][snapshot] and no [overlayfs][overlayfs] is present.
Confirm and exit
Check your partitions
save and exit

### Bootloader
- [GRUB][grub] since we will use [`grub-btrfs`][grub-btrfs] to create bootmenus for snapshots

~~~
Bootloader
  GRUB = yes
~~~

### Hostname
~~~
Hostname = <HOSTNAME>
~~~

### Root account
~~~
Root passwort = none
~~~

### User account 
- Create with [`sudo`][sudo] priviliges

~~~
User account
  Add a user
    Enter username = <USERNAME>
    Password = <PASSWORT>

    Superuser = yes
~~~

### GNOME and open-source graphic driver
~~~
Profile 
  desktop
    gnome
      Graphics driver = All open-source
~~~

### Audio-backend
~~~
Audio = pipewire
~~~

### Kernel
~~~
Kernels = linux-zen
~~~

### Additional packages
~~~
[amd|intel]-ucode cups firefox firefox-i18n-de flatpak git inotify-tools nano noto-fonts reflector ttf-dejavu ttf-jetbrains-mono ttf-liberation dconf-editor file-roller p7zip unrar gedit gedit-plugins seahorse gnome-terminal gnome-tweaks libreoffice-fresh libreoffice-fresh-de meld gimp inkscape gparted dosfstools btrfs-progs ntfs-3g polkit gpart mtools xorg-xhost vlc lib32-gnutls wine
~~~

### Networking
~~~
Network configuration = NetworkManager
~~~

### Timezone
~~~
Timezone = Europe/Berlin
~~~

### Optional repositories
~~~
Optional repositories = multilib
~~~
### Start the installation
##### no way back after this
~~~
Install
~~~
- Wait for the installer to finish without error

## ***Part 2:*** chroot-environment

### chroot
~~~
chroot = yes
~~~

### fstab
~~~
# /etc/fstab

# remove this from all btrfs-partitions 
      space_cache=v2,subvolid=<ID>

#add to all btrfs-partitions
      compress=zstd
~~~

### Compress btrfs
~~~sh
btrfs filesystem defragment -r -v -czstd /
~~~

### systemd services
- Archlinux doesn't enable these services by default

~~~sh
systemctl enable avahi-daemon
systemctl enable bluetooth
systemctl enable cups
systemctl enable upower
systemctl enable sshd
~~~

### Configure and enable Reflector
Uncomment and change

~~~
# /etc/xdg/reflector/reflector.conf

--Country <YOUR_COUNTRY>
--sort rate
~~~
enable [systemd][systemd] timer

~~~sh
systemctl enable reflector.timer
~~~

### Exit chroot and reboot
~~~sh
exit
reboot
~~~

## ***Part 3:*** Inside the new ArchLinux system

### Check /etc/fstab
~~~sh
mount | grep home
~~~
`compress=zstd:3` and `space_cache=v2` should be present. Otherwise check [fstab](#fstab)

### Update the system
~~~sh
sudo pacman -Syy
sudo pacman -Syu
~~~

### KVM/QEMU
- If the system resides within a virtual machine, enable dynamic resolution and clipboard-sharing

~~~sh
sudo pacman -S spice-vdagent
~~~

Reboot the system

~~~sh
reboot
~~~

### Enable Arch User Repository (AUR)

~~~sh
git clone https://aur.archlinux.org/yay
cd yay
makepkg -si
cd ..
rm -rf yay

yay -Y --gendb #generate a development package database for *-git packages
yay -Syu --devel
yay -Y --devel --combinedupgrade --batchinstall --editmenu --nodiffmenu --save
~~~
- Take a look at the [yay Installation guide][yay]

### Create and manage snapshots

- Install the [`snapper-support`][snapper-support] metapackage.
This will pull and configure [`snapper`][snapper], [`grub-btrfs`][grub-btrfs] and [`snap-pac`][snap-pac].

~~~sh
yay -S snapper-support
~~~

- **IMPORTANT: `snapper-support` fails to create a btrfs subvolume.**</br>**This is expected.**</br>**Go to [Fix snapshots](#fix-snapshots)**

- **IMPORTANT: [`grub-btrfs`][grub-btrfs] doesn't work with `systemd` ([issue #199][issue #199]).**</br>**This is expected.**</br>**Go to [Fix initial ramdisk](#fix-initial-ramdisk)**


### Fix initial ramdisk

- [`grub-btrfs`][grub-btrfs] doesn't work with `systemd` ([issue #199][issue #199])

Replace in HOOKS:

~~~
# /etc/mkinitcpio.conf

'systemd' with 'udev'
'sd-vconsole' with 'keymap consolefont'
~~~
Add to BINARIES:

~~~
# /etc/mkinitcpio.conf

setfont
~~~
Change /etc/vconsole.conf

Add the following line. `eurlatgr` is a better default console font for European based languages.

~~~
# /etc/vconsole.conf

FONT=eurlatgr
~~~
- Regenerate initial ramdisk

~~~sh
sudo mkinitcpio -P
~~~


### Fix snapshots

~~~sh
# umount and delete ./snapshots
sudo umount /.snapshots
sudo rm -r /.snapshots

# create a new configuration 
sudo snapper -c root create-config /

# delete /.snapshots subvolume and create /.snapshots
sudo btrfs subvolume delete /.snapshots

# check if /.snapshots is gone and @snapshots is present
sudo btrfs subvolume list /  

# create and mount /.snapshots
sudo mkdir /.snapshots
sudo mount -av
~~~
- A good practice is to reboot now

~~~sh
reboot
~~~

### Change btrfs default subvolume to @
- The default subvolume should not point to '/'

~~~sh
sudo btrfs subvol get-default /
~~~
this should return somthing like

~~~
ID 5 (FS_TREE)
~~~
we need to change that

~~~sh
sudo btrfs subvol list /
# take a note of the ID of subvolume @ (possibly 256)
# change the command accordingly
sudo btrfs subvolume set-default 256 /
# check again with
sudo btrfs subvol get-default /
~~~

### Enable swap
- @swap is already mounted at /swap. See [Chapter 8. Disk layout](#disk-layout)

~~~sh
btrfs filesystem mkswapfile --size 4g --uuid clear /swap/swapfile
swapon /swap/swapfile
~~~
edit the [fstab][fstab] configuration to add an entry for the swap file:

~~~
/etc/fstab

/swap/swapfile none swap defaults 0 0
~~~
### Change permissions for snapper and /.snapshots
- Allow members of [`wheel`][wheel] access to snapper

~~~
# /etc/snapper/configs/root

ALLOW_GROUPS="wheel"
~~~
- Allow members of [`wheel`][wheel] access to /.snapshots

~~~sh
sudo chown -R :wheel /.snapshots
~~~
### Set snapshot limits
- Set limits to the Arch-wiki [recommendation][snapshot-limits]

~~~
# /etc/snapper/configs/root

TIMELINE_MIN_AGE="1800"
TIMELINE_LIMIT_HOURLY="5"
TIMELINE_LIMIT_DAILY="7"
TIMELINE_LIMIT_WEEKLY="0"
TIMELINE_LIMIT_MONTHLY="0"
TIMELINE_LIMIT_YEARLY="0"
~~~

### Preventing slowdowns
- See [preventing slowdowns][preventing-slowdowns] in the Arch-wiki











[1]: https://wiki.archlinux.org/title/Installation_guide
[archinstall]: https://wiki.archlinux.org/title/archinstall
[gdm]: https://wiki.archlinux.org/title/GDM
[snapshot]: https://wiki.archlinux.org/title/Btrfs#Snapshots
[overlayfs]: https://wiki.archlinux.org/title/Overlay_filesystem
[grub]: https://wiki.archlinux.org/title/GRUB
[sudo]: https://wiki.archlinux.org/title/Sudo
[reflector]: https://wiki.archlinux.org/title/Reflector
[systemd]: https://wiki.archlinux.org/title/Systemd
[chroot]: https://wiki.archlinux.org/title/Chroot
[yay]: https://github.com/Jguer/yay#installation
[snapper-support]: https://aur.archlinux.org/packages/snapper-support
[snapper]: https://wiki.archlinux.org/title/snapper
[grub-btrfs]: https://archlinux.org/packages/?name=grub-btrfs
[snap-pac]: https://archlinux.org/packages/?name=snap-pac
[issue #199]: https://github.com/Antynea/grub-btrfs/issues/199
[fstab]: https://wiki.archlinux.org/title/Fstab
[snap-pac]: https://archlinux.org/packages/?name=snap-pac
[snapshot-limits]:https://wiki.archlinux.org/title/snapper#Set_snapshot_limits
[wheel]: https://wiki.archlinux.org/title/Users_and_groups#Group_list
[preventing-slowdowns]: https://wiki.archlinux.org/title/snapper#Preventing_slowdowns